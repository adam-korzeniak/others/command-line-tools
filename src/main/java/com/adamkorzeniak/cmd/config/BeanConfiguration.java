package com.adamkorzeniak.cmd.config;

import com.adamkorzeniak.json.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public JsonParser jsonParser() {
        return new JsonParser(objectMapper());
    }
}
