package com.adamkorzeniak.cmd.json;

import com.adamkorzeniak.file.FileReader;
import com.adamkorzeniak.json.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

@Service
@RequiredArgsConstructor
public class ParserService {

    private final ObjectMapper objectMapper;
    private final JsonParser jsonParser;
    private final ParsingConfiguration parsingConfiguration;

    public String parse(ParsingExpression expression) {
        String jsonString = readFileContent(parsingConfiguration.getInput());
        Map<String, Object> jsonModel;
        try {
            jsonModel = jsonParser.fromJsonToMap(jsonString);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Could not process");
        }

        Map<String, Object> model = Optional.of(expression)
                .map(ParsingExpression::getSelectExpression)
                .filter(Predicate.not(String::isBlank))
                .map(expr -> select(expr, jsonModel))
                .orElse(jsonModel);

        //selectExpression = id,user.contact,user.contact.phone
        //orderExpression = id,user.contact asc,user.contact.phone
        //filterExpression =
        // id eq 1,
        // id neq 2,
        // user.contact ex,
        // user.contact.phone nex,
        // user.rating gt 1,
        // user.rating lt 2za,
        // user.rating le 2,
        // user.rating ge 3
        // user.name like slaw
        // user.name nlike slaw

//        JsonUtils.filterElements(jsonModel, filterExpression);
//        JsonUtils.orderElements(jsonModel, orderExpression);
//        JsonUtils.selectFields(jsonModel, selectExpression);
//        String jsonResultString = JsonUtils.convertToString(jsonModel);
//        FileUtils.outputContent(defaultOutputFilePath, jsonResultString);
        try {
            return objectMapper.writeValueAsString(model);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    private Map<String, Object> select(String expr, Map<String, Object> jsonModel) {
        String[] elements = expr.trim().split(",");
        Map<String, Object> newModel = new HashMap<>();
        for (String el2 : elements) {
            String el = el2.trim();
            String[] tokens = el.split("\\.");
            getValue(jsonModel, el)
                    .ifPresent((value) -> newModel.put(el, value));
        }
        return newModel;
    }

    private Optional<Object> getValue(Map<String, Object> jsonModel, String key) {
        if (jsonModel.containsKey(key)) {
            return Optional.ofNullable(jsonModel.get(key));
        }
        return Optional.empty();
    }

    private String readFileContent(String filePath) {
        try {
            return FileReader.readFileContent(filePath);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Could not open file.%nException: %s", e));
        }
    }
}
