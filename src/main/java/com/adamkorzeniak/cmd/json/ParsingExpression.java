package com.adamkorzeniak.cmd.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ParsingExpression {
    private String selectExpression;
    private String filterExpression;
    private String orderExpression;
}
