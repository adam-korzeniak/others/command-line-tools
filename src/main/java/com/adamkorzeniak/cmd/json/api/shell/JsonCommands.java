package com.adamkorzeniak.cmd.json.api.shell;

import com.adamkorzeniak.cmd.json.ParserService;
import com.adamkorzeniak.cmd.json.ParsingExpression;
import com.adamkorzeniak.cmd.json.domain.ExpressionTokenizer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

@ShellComponent
@RequiredArgsConstructor
public class JsonCommands {

    private final ObjectMapper objectMapper;
    private final ParserService parserService;
    private final ExpressionTokenizer expressionTokenizer;

    @ShellMethod(key = {"pj", "parse-json"}, value = "Parse and modify JSON file")
    public String parseJson(
            @ShellOption(value = {"-s", "--select"}, defaultValue = "") String selectExpression,
            @ShellOption(value = {"-f", "--filter"}, defaultValue = "") String filterExpression,
            @ShellOption(value = {"-o", "--order"}, defaultValue = "") String orderExpression
    ) throws JsonProcessingException {
        ParsingExpression expression = new ParsingExpression(selectExpression, filterExpression, orderExpression);
//        return parserService.parse(expression);
        return objectMapper.writeValueAsString(expressionTokenizer.tokenizeSelectExpression(selectExpression).getItems());
    }

}