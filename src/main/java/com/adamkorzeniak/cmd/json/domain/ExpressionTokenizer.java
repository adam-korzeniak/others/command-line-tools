package com.adamkorzeniak.cmd.json.domain;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ExpressionTokenizer {

    public SelectExpression tokenizeSelectExpression(String selectExpression) {
        SelectExpressionBuilder builder = new SelectExpressionBuilder();
        Map<Boolean, List<String>> items = Stream.of(selectExpression.trim().split(","))
                .map(String::trim)
                .filter(Predicate.not(String::isBlank))
                .map(this::validate)
                .collect(Collectors.partitioningBy(item -> item.startsWith("!")));
        items.get(true).forEach(item -> builder.exclude(item.replace("!", "")));
        items.get(false).forEach(builder::include);
        return builder.build();
    }

    private String validate(String item) {
        if (item.lastIndexOf("!") > 0) {
            throw new RuntimeException("! can only be placed before field. Valid use: \"!name,!address.city\". Invalid use \"! name,address.!city\" ");
        }
        if (item.charAt(item.length() - 1) == '.') {
            throw new RuntimeException("Field name cannot end with \".\". Valid use: \"person.name,!address.city\". Invalid use \"! address.,person.\" ");
        }
        String invalidCharacters = item.replaceAll("[\\w.!]+", "");
        if (!invalidCharacters.isEmpty()) {
            throw new RuntimeException("Invalid characters: " + invalidCharacters);
        }
        return item;
    }


}
