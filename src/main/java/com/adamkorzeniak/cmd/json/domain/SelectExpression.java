package com.adamkorzeniak.cmd.json.domain;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.Collections;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Value
public class SelectExpression {
    private final Set<Item> items;

    @Value
    @EqualsAndHashCode
    public static class Item {
        private final String key;
        private final boolean included;
    }

    public static SelectExpression empty() {
        return new SelectExpression(Collections.emptySet());
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public Set<String> getIncluded() {
        return items.stream()
                .filter(item -> item.included)
                .map(Item::getKey)
                .collect(Collectors.toSet());
    }

    public Set<String> getExcluded() {
        return items.stream()
                .filter(Predicate.not(item -> item.included))
                .map(Item::getKey)
                .collect(Collectors.toSet());
    }
}
